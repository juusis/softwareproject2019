## Application

Application's purpose is to make [ASC](https://github.com/ouspg/ASC) easier to integrate into a development pipeline by providing
a wrapper around it that handles things like saving har files and showing output in more human readable way.

Application uses mitmproxy software to save a HAR file that is generated during a testing of an OpenAPi api.
Application then uses ASC externally with this generated
HAR file and shows the output of ASC to the user.
There are two options for the output: GUI output or a simple print of the ASC output to the command line interface.

### Dependencies

Mitmproxy software https://mitmproxy.org/

Instructions on how to install the certificate for mitmproxy https://docs.mitmproxy.org/stable/concepts-certificates/

Api specification coverage tool https://github.com/ouspg/ASC
Dependencies of the ASC need to be installed system wide because the current version of the application
uses python without virtual environment when running the ASC subprocess. Application uses "python" path variable
when running the ASC so if you have multiple versions of python installed you might need to modify the command
inside the version of the application you are running because it might default to wrong version of python.

Python 3.7 and several python libraries that are in the requirements.txt


# Running the application:
Versions for Windows and Unix with or without GUI output can be found inside src/python/mitmproxy directory

OpenAPI specification of the API you are testing needs to be provided when running the application as well as basepath and serveraddress
if they are different than in the OpenAPI specification file or in the configuration file of the ASC.

Basepath and Serveraddress are provided by commandline arguments as seen in example below:

`python windows_no_GUI.py petstore.json --basepath /v2 --serveraddress petstore.swagger.io`

After the user has ran the application and the proxy is listening, user can run the tests to their API. After the tests are done,
keyboard interrupt is used to close the proxy which then forwards the generated HAR file to the ASC and the output of the ASC is generated.

## Unix version

In the Unix version proxy is setup by the user after running the software. For example, if you are running your tests to your API from Unix shell,
you can simply use command:

`export http_proxy="http://localhost:8080"`

after which your test traffic will go through the proxy that application is listening.


## Windows version

In the Windows version proxy is setup automatically by application changing the registry keys. There is a bug in the current version which leads
to proxy sometimes not starting. This can be fixed by opening Network & Internet settings menu on windows 10. The suspected main cause for this bug is that
while we do change the registry key values they need to be read from the registry by some application before they take effect.
By opening the the settings, values are read and system wide proxy becomes active.

# Running tests
To run te unit tests run:
`python tests.py`
