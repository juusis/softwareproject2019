import requests
import schemathesis
from hypothesis import settings

"""
Does test on the online API that is hosted by swagger themselves.
"""

BASE_URL = "http://petstore.swagger.io"
schema = schemathesis.from_uri(f"{BASE_URL}/v2/swagger.json")
# schema = schemathesis.from_file("D:\openapiplugin\Testdata\petstore_v2.json")

@schema.parametrize()
@settings(deadline=None, max_examples=5)
def test_no_server_errors(case):
    # `requests` will make an appropriate call under the hood
    response = case.call(BASE_URL)
    assert response.status_code < 500
