import requests
import schemathesis
from hypothesis import settings

"""
Does tests on a petstore API that is ran in localhost port 80

For example, with docker:

docker pull swaggerapi/petstore3:unstable
docker run -d -p 80:8080 swaggerapi/petstore3

"""
BASE_URL = "http://localhost/api/v3"
schema = schemathesis.from_uri(f"{BASE_URL}/openapi.json")
# schema = schemathesis.from_file("D:\openapiplugin\Testdata\petstore_v2.json")

@schema.parametrize()
@settings(deadline=None, max_examples=5)
def test_no_server_errors(case):
    # `requests` will make an appropriate call under the hood
    response = case.call(BASE_URL)
    assert response.status_code < 500
