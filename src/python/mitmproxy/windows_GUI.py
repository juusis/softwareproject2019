import subprocess
import argparse
import os
import winreg
import gui

# store old proxy values
migrateProxy = 0
proxyEnable = 0
proxyServer = ""



def set_proxy(reg_key):
    global migrateProxy
    global proxyEnable
    global proxyServer

    try:
        migrateProxy = winreg.QueryValueEx(reg_key, "MigrateProxy")[0]
        proxyEnable = winreg.QueryValueEx(reg_key, "ProxyEnable")[0]
        proxyServer = winreg.QueryValueEx(reg_key, "ProxyServer")[0]

        winreg.SetValueEx(reg_key, "MigrateProxy", 0, winreg.REG_DWORD, 1)
        winreg.SetValueEx(reg_key, "ProxyEnable", 0, winreg.REG_DWORD, 1)
        winreg.SetValueEx(reg_key, "ProxyServer", 0, winreg.REG_SZ, "http://localhost:8080")

    except FileNotFoundError:
        print("Registry file not found, please enable proxy from windows settings atleast once")
        exit()

def reset_proxy(reg_key):
    winreg.SetValueEx(reg_key, "MigrateProxy", 0, winreg.REG_DWORD, migrateProxy)
    winreg.SetValueEx(reg_key, "ProxyEnable", 0, winreg.REG_DWORD, proxyEnable)
    winreg.SetValueEx(reg_key, "ProxyServer", 0, winreg.REG_SZ, proxyServer)




def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('json', type=str, help='The Open API specification file')
    parser.add_argument('--serveraddress', type=str, help='The address of server', default='')
    parser.add_argument('--basepath', type=str, help='The basepath of server. Eg: /v2, /v3, etc.', default='')
    args = parser.parse_args()

    if not os.path.isfile(args.json) or not args.json.endswith(".json"):
        raise FileNotFoundError("No json file found at the path")

    print("Starting up the proxy server...")
    reg_key = winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings", access=winreg.KEY_ALL_ACCESS)
    set_proxy(reg_key)

    try:
        subprocess.run("mitmdump -w dump.mitm -s ./hardump.py --set hardump=./dump.har --set block_global=false")
    except KeyboardInterrupt:
        pass

    print("Proxy closed. Creating HAR file...")
    reset_proxy(reg_key)
    subprocess.run("mitmdump -n -r dump.mitm -s hardump.py --set hardump=./dump.har")

    print("Running the ASC tool...")
    subprocess.run("python ../../../ASC/ASC.py --serveraddress {0} --basepath {1} --cf ../../../ASC/config.ini {2} dump.har".format(args.serveraddress, args.basepath, args.json))

if __name__ == '__main__':
    main()
    gui.main()
