import unittest
import sys
import unix_GUI
import gui
from PySide2 import (QtGui, QtTest, QtWidgets)

# Skips Windows import if OS is not Windows
try:
	import winreg
	import windows_GUI
except ImportError:
    pass
    
class Test_proxy(unittest.TestCase):

	def setUp(self):
		super(Test_proxy, self).setUp()

		# Setups the instance for GUI
		# See https://stackoverflow.com/questions/50005373/runtimeerror-please-destroy-the-qapplication-singleton-before-creating-a-new-qa
		if isinstance(QtGui.qApp, type(None)):
			self.app = QtWidgets.QApplication(sys.argv)
		else:
			self.app = QtGui.qApp

		self.gui = gui.Form()

	'''
	Proxy tests
	'''
	# Test setup succeeds with Windows platform
	@unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
	def test_setup_windows(self):
		try:
			reg_key = winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings", access=winreg.KEY_ALL_ACCESS)
			windows_GUI.set_proxy(reg_key)
		except FileNotFoundError:
			self.fail("Error setting up proxy: FileNotFoundError.")

	# Test setup with bad key
	@unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
	def test_setup_windows_bad_key(self):
		self.assertRaises(SystemExit, windows_GUI.set_proxy, winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, ""))

	# Test reseting proxy
	@unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
	def test_reset(self):
		reg_key = winreg.OpenKeyEx(winreg.HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings", access=winreg.KEY_ALL_ACCESS)
		windows_GUI.reset_proxy(reg_key)


	# Test same with Liux platform
	@unittest.skipUnless(sys.platform.startswith("linux"), "requires Linux")
	def test_setup_linux(self):
		try:
			unix_GUI.main()
		except FileNotFoundError:
			self.fail('Error setting up proxy: FileNotFoundError')

	'''
	GUI tests
	'''
	def test_setup(self):
		self.assertIsNotNone(self.gui.layout)

	# Button tests
	def test_buttons(self):
		self.assertIs(type(self.gui.layout.itemAt(0).widget()), QtWidgets.QPushButton)

	def tearDown(self):
		del self.app
		return super(Test_proxy, self).tearDown()

if __name__ == '__main__':
	unittest.main()
