from mitmproxy.tools import _main
import subprocess
import argparse
import os
import gui

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('json', type=str, help='The Open API specification file')
    parser.add_argument('--serveraddress', type=str, help='The address of server', default='')
    parser.add_argument('--basepath', type=str, help='The basepath of server. Eg: /v2, /v3, etc.', default='')
    args = parser.parse_args()

    if not os.path.isfile(args.json) or not args.json.endswith(".json"):
        raise FileNotFoundError("No json file found at the path")

    print("Starting up the proxy server...")
    try:
        _main.mitmdump(["-s", "hardump.py", "--set", "hardump=dump.har", "--set", "block_global=false"])
    except KeyboardInterrupt:
        pass

    print("Proxy closed. Running ASC tool...")

    subprocess.run("python ../../../ASC/ASC.py --serveraddress {0} --basepath {1} --cf ../../../ASC/config.ini {2} dump.har".format(args.serveraddress, args.basepath, args.json), shell=True)

if __name__ == '__main__':
    main()
    gui.main()
