import sys
import json
from math import sqrt
from PySide2.QtWidgets import (QTextEdit, QPushButton, QApplication,
    QVBoxLayout, QDialog, QGridLayout, QSpacerItem)

class PopupJson(QDialog):

    def __init__(self, parent=None):
        super(PopupJson, self).__init__(parent)

        self.styleTextbox = ("""QWidget {
            font-size: 14px;
            width: 600px;
            height: 300px;
        }""")

        self.textbox = QTextEdit()
        self.textbox.setStyleSheet(self.styleTextbox)
        self.textbox.setText(json.dumps(data, indent=4))
        self.textbox.setReadOnly(True)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.textbox)

        self.setLayout(self.layout)

class Popup(QDialog):

    def __init__(self, parent=None):
        super(Popup, self).__init__(parent)

        self.stylesheetButton = ("""QWidget {
            background-color: #ccc;
            border-style: outset;
            border-width: 0px;
            border-color: beige;
            font-size: 22px;
            height: 100%;
            width: 128px;
        }""")
        self.stylesheetButtonGreen = ("""QWidget {
            background-color: #5EB164;
            border-style: outset;
            border-width: 0px;
            border-color: beige;
            font-size: 22px;
            height: 100%;
            width: 128px;
        }""")
        self.stylesheetButtonRed = ("""QWidget {
            background-color: #BC5C5C;
            border-style: outset;
            border-width: 0px;
            border-color: beige;
            font-size: 22px;
            height: 100%;
            width: 128px;
        }""")

        self.methods = data["methods"]
        self.layout = QVBoxLayout()

        self.CreateButtons()

        self.setLayout(self.layout)

    def CreateButtons(self):
        """ Creates method buttons dynamically based on data. """

        buttons = []
        for i, j in self.methods.items():
            button = QPushButton(i)
            button.clicked.connect(self.showData)
            if j["response_codes_count"] == j["response_codes_used"] and j["parameters_used"] == j["parameters_count"] and j["default_response_exists"] == False and j["anomalies_count"] == 0:
                button.setStyleSheet(self.stylesheetButtonGreen)
            elif j["response_codes_used"] == 0 and j["parameters_used"] == 0:
                button.setStyleSheet(self.stylesheetButton)
            else:
                button.setStyleSheet(self.stylesheetButtonRed)

            self.layout.addWidget(button)

    def showData(self):
        """ Runs when a button is clicked.
        Identifies which button is clicked with self.sender() and assigns data to match the given caller.
        Opens the method Json data popup. """

        global data
        sender = self.sender()

        for i, j in self.methods.items():
            if i == sender.text():
                data = j

        popup = PopupJson()
        popup.exec()

class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.stylesheetButton = ("""QWidget {
            background-color: #ccc;
            border-style: outset;
            border-width: 0px;
            border-color: beige;
            font-size: 22px;
            height: 100%;
        }""")
        self.stylesheetButtonGreen = ("""QWidget {
            background-color: #5EB164;
            border-style: outset;
            border-width: 0px;
            border-color: beige;
            font-size: 22px;
            height: 100%;
        }""")
        self.stylesheetButtonRed = ("""QWidget {
            background-color: #BC5C5C;
            border-style: outset;
            border-width: 0px;
            border-color: beige;
            font-size: 22px;
            height: 100%;
        }""")


        with open("large_report_json.json", "r") as f:
            jsonData = json.load(f)

        self.layout = QGridLayout()

        self.endpoints = []
        self.endpoints = jsonData["endpoints"]

        self.CreateButtons(self.endpoints)

        self.setLayout(self.layout)

    def showData(self):
        """ Runs when a button is clicked.
        Identifies which button is clicked with self.sender() and assigns data to match the given caller.
        Opens the endpoints method popup. """

        global data
        sender = self.sender()

        for endpoint in self.endpoints:
            if endpoint["path"] == sender.text():
                data = endpoint

        popup = Popup()
        popup.exec()

    def AddButtonsToLayout(self, buttons, r):
        """Generate a grid with a dimension r, e.g if r=2, a 2x2 grid is generated. Populate grid layout with buttons"""
        for x in range(r):
            for y in range(r):
                k = x + r * y
                k = min(len(buttons) - 1, k)
                self.layout.addWidget(buttons[k], y, x)

    def CreateButtons(self, endpoints):
        """ Create endpoint UI buttons based on given endpoint list. Styles buttons according to endpoint test data. """

        buttons = []
        i = 0
        for endpoint in endpoints:
            buttons.append(QPushButton(endpoint["path"]))
            anomalies = False

            for key, value in endpoint["methods"].items():
                if value["anomalies_count"] > 0:
                    anomalies = True

            if(endpoint["methods_count"] == endpoint["methods_used"] and endpoint["response_codes_in_methods_used"] == endpoint["response_codes_in_methods_count"] and endpoint["parameters_in_methods_used"] == endpoint["parameters_in_methods_count"] and anomalies == False):
                buttons[i].setStyleSheet(self.stylesheetButtonGreen)
            elif(endpoint["usage_count"] == 0):
                buttons[i].setStyleSheet(self.stylesheetButton)
            else:
                buttons[i].setStyleSheet(self.stylesheetButtonRed)
            buttons[i].clicked.connect(self.showData)
            i += 1

        r = sqrt(len(endpoints))
        r = round(r)

        self.AddButtonsToLayout(buttons, r)

def main():
    """ Call this function to run the UI """

    app = QApplication(sys.argv)
    form = Form()
    form.show()
    sys.exit(app.exec_())
